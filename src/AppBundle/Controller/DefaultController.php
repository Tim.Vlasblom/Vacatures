<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Entity\Vacancy;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $vacancies = $em->getRepository('AppBundle:Vacancy')->findAll();

        return $this->render('default/index.html.twig', array(
            'vacancies' => $vacancies,
        ));
    }

    /**
     * @Route("/new", name="new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $vacancy = new Vacancy();
        $form = $this->createForm('AppBundle\Form\VacancyType', $vacancy);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($vacancy);
            $em->flush();

            return $this->redirectToRoute('index');
        }

        return $this->render('default/new.html.twig', array(
            'vacancy' => $vacancy,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/delete/{id}", name="delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Vacancy $vacancy)
    {
        
            $em = $this->getDoctrine()->getManager();
            $em->remove($vacancy);
            $em->flush();
        

        return $this->redirectToRoute('index');
    }

    /**
     * @Route("/{id}/edit", name="edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Vacancy $vacancy)
    {
        $editForm = $this->createForm('AppBundle\Form\VacancyType', $vacancy);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('edit', array('id' => $vacancy->getId()));
        }

        return $this->render('default/edit.html.twig', array(
            'vacancy' => $vacancy,
            'edit_form' => $editForm->createView(),
        ));
    }
}
